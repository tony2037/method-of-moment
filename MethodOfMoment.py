import math
from sympy.solvers import solve
from sympy import Symbol, Eq
N = 7 # How many charges chosen on a radius, default 2
slides = 4 # How many slides we choose
Potential_top = 0.5 # The potential of the top disk
Potential_bottom = -0.5 # The potential of the bottom disk
a = 1 # The radius of the disk, default 1
diameter = a # Diameter
radius = diameter/2
distance = a/ 10 # The distance between the two disk
num_sets = 2 **(slides) # How many set
Charges = [] # An array to store the charges
Test_points = [] # Test points
Equations = [] # The eqations to be solved
k_e = 8.89755178736* (10**9) # The coefficient : 1/(4* pi* esilon)
Symbols = [] # Which are the answers
StandardAns_C1 = 0.824* (1/k_e)* a #0.824×(4πεa)
class Charge():
    def __init__(self, r, sita, z, charge = None):
        self.r = r
        self.sita = sita
        self.z = z
        self.charge = charge

def Initilize():
    for i in range(0, 2* N):
        Symbols.append(Symbol('Q%s' % i))
    
    for i in range(0, num_sets):
        current_sita = 360/ num_sets * i
        for j in range(0, N):
            current_r = radius/ N* (j+1)
            Charges.append(Charge(current_r, current_sita, distance, j))
            Charges.append(Charge(current_r, current_sita, 0, N+j))
            if(current_sita == 0):
                Test_points.append((current_r- radius/ (2* N), 0, distance))
                Test_points.append((current_r- radius/ (2* N), 0, 0))
    for c in Charges:
        print('Charge: (%s, %s, %s)' % (c.r, c.sita, c.z))
    for t in Test_points:
        print('Test points: %s' % str(t))
    for s in Symbols:
        print(s)
    assert len(Charges) == 2* N* num_sets
    assert len(Test_points) == 2* N


def distance_test2charge(Q, test):
    x1 = Q.r* math.cos(math.radians(Q.sita))
    y1 = Q.r* math.sin(math.radians(Q.sita))
    z1 = Q.z
    x2 = test[0]
    y2 = 0 # Because the test points are always at sita = 0 
    z2 = test[-1]
    return math.sqrt((x1- x2)**2 + (y1- y2)**2 + (z1- z2)**2)

def contruct_Equations():
    for t in Test_points:
        if(t[-1] == distance): # Top, potential top
            eq = 0
            for c in Charges:
                eq += k_e* (1/ distance_test2charge(c, t))* Symbols[c.charge]
            Equations.append(Eq(eq, Potential_top))
        elif (t[-1] == 0): # Buttom, potential buttom
            eq = 0
            for c in Charges:
                eq += k_e* (1/ distance_test2charge(c, t))* Symbols[c.charge]
            Equations.append(Eq(eq, Potential_bottom))
        else:
            print('error')
    return solve(Equations, Symbols)

def total_Charge(Ans):
    total_Charge_top = 0
    total_Charge_bottom = 0
    for c in Charges:
        if(c.z == 0):
            total_Charge_bottom += Ans[Symbols[c.charge]]
        else:
            total_Charge_top += Ans[Symbols[c.charge]]
    return (total_Charge_top, total_Charge_bottom)

if __name__ == '__main__':
    Initilize()
    Ans = contruct_Equations()
    print(Ans)
    print('Standard answer C1: ' + str(StandardAns_C1))
    (C1, C2) = total_Charge(Ans)
    print('Moment Method answer C1: ' + str(C1))
