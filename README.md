The method of moment
===

# The Problem: 
* (a) Two conducting circular disks, with diameter of a, are put in parallel and separated by a/10, as a capacitor. Their voltages are +0.5 and -0.5 volts, respectively. Write a simple program based on method of moment to find the total charges on each disk. Note that this is also the capacitance of the parallel disks. Let's call it C1. 
(The answer is 0.824×(4πεa))


* (b) Repeat part (a) by increasing the separation to a/5 and find the total charges on each disk. Let's call it C2. 


* (c) The capacitor of part (b) is effectively two capacitors of part (a) in series. Then, is C2 one half of C1, as you have learned in your basic circuit class?
```
9.9/ 5.725 = 1.729
It’s not exactly 2. But 1.729
```

1. How are the charges approximated? (i.e., where you put the charges if you use point-charge approximation) 

Where:
Circles are the charges put on the disk
Xs are the test points.
And the **space is radius/ (2* N)**

2. How are the voltages tested? (i.e., where you force the voltage to be the correct value) 

* Firstly, Pick a test point, and calculate the potential contributed by each charge and add them up.
* Secondly, by the experience, the fact that the potential is a kind of scalar that can be added up directly.

3. Your program (with short explanations)
 https://gitlab.com/tony2037/method-of-moment/blob/master/MethodOfMoment.py
The data structure for the charges
```python=
class Charge():
    def __init__(self, r, sita, z, charge = None):
        self.r = r
        self.sita = sita
        self.z = z
        self.charge = charge
```

The explanations with the parameters
```python=
N = 6 # How many charges chosen on a radius, default 2
slides = 4 # How many slides we choose
Potential_top = 0.5 # The potential of the top disk
Potential_bottom = -0.5 # The potential of the bottom disk
a = 1 # The radius of the disk, default 1
diameter = a # Diameter
radius = diameter/2
distance = a/ 10 # The distance between the two disk
num_sets = 2 **(slides) # How many set
Charges = [] # An array to store the charges
Test_points = [] # Test points
Equations = [] # The eqations to be solved
k_e = 8.89755178736* (10**9) # The coefficient : 1/(4* pi* esilon)
Symbols = [] # Which are the answers
StandardAns_C1 = 0.824* (1/k_e)* a #0.824×(4πεa)
``` 

Figure out the distance from a test point to a charge
```python=
def distance_test2charge(Q, test)
```
4. A graph shows the calculated total charges on each disk converge to the right answer in part (a) when you increase the number of unknowns. 
N = 5

N = 6

N = 7

5. A similar graph shows the calculated total charges on each disk of part (b) converge to your final answer. 
N = 5

N = 6

N = 7

6. Is C2 equal to 0.5*C1. If not, what is the percentage error using basic circuit formula to calculate C2? 
```
1.729 / 2 * 100 % = 86.45%
```